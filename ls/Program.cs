﻿using System;
using System.IO;


namespace ls
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			ConsoleColor iniColor = Console.ForegroundColor;
			Print(args);			
			Console.ForegroundColor = iniColor;
		}

		static void Print(string[] args)
		{
			string[] dirs;
			dirs = Directory.GetDirectories(Directory.GetCurrentDirectory());

			foreach (string idir in dirs)
			{
				Console.ForegroundColor = ConsoleColor.Yellow;
				Console.WriteLine(new DirectoryInfo(idir).Name);
			}

			string[] Files;
			if (args.Length > 0)
				Files = Directory.GetFiles(Directory.GetCurrentDirectory(), args[0]);
			else
				Files = Directory.GetFiles(Directory.GetCurrentDirectory());
			foreach (string iFile in Files)
			{
				Console.ForegroundColor = ConsoleColor.Green;
				Console.WriteLine(Path.GetFileName(iFile));
			}

		}
	}
}